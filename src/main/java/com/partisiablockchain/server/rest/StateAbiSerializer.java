package com.partisiablockchain.server.rest;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateBoolean;
import com.partisiablockchain.serialization.StateLong;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.serialization.StateString;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.ByteOutput;
import com.secata.stream.LittleEndianByteOutput;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.immutable.FixedList;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/** Serializing state into abi state format. */
@SuppressWarnings("EnumOrdinal")
public final class StateAbiSerializer {
  static Map<Class<?>, StateAbiWriter> SIMPLE_TYPES;

  static {
    HashMap<Class<?>, StateAbiWriter> map = new HashMap<>();
    map.put(Integer.TYPE, (o, out) -> out.writeI32((int) o));
    map.put(Integer.class, (o, out) -> out.writeI32((int) o));
    map.put(Boolean.TYPE, (o, out) -> out.writeBoolean((boolean) o));
    map.put(Boolean.class, (o, out) -> out.writeBoolean((boolean) o));
    map.put(StateBoolean.class, (o, out) -> out.writeBoolean(((StateBoolean) o).value()));
    map.put(Byte.TYPE, (o, out) -> out.writeI8((byte) o));
    map.put(Byte.class, (o, out) -> out.writeI8((byte) o));
    map.put(Short.TYPE, (o, out) -> out.writeI16((short) o));
    map.put(Short.class, (o, out) -> out.writeI16((short) o));
    map.put(Long.TYPE, (o, out) -> out.writeI64((long) o));
    map.put(Long.class, (o, out) -> out.writeI64((long) o));
    map.put(StateLong.class, (o, out) -> out.writeI64(((StateLong) o).value()));
    map.put(String.class, (o, out) -> out.writeString((String) o));
    map.put(StateString.class, (o, out) -> out.writeString(((StateString) o).value()));
    map.put(
        BlockchainAddress.class,
        (o, out) -> {
          BlockchainAddress address = (BlockchainAddress) o;
          out.writeI8(address.getType().ordinal());
          out.writeBytes(address.getIdentifier());
        });
    map.put(
        LargeByteArray.class,
        (o, out) -> {
          LargeByteArray bytes = (LargeByteArray) o;
          out.writeI32(bytes.getLength());
          out.writeBytes(bytes.getData());
        });
    map.put(
        Unsigned256.class,
        (o, out) ->
            out.writeUnsignedBigInteger(new BigInteger(1, ((Unsigned256) o).serialize()), 32));
    map.put(
        BlockchainPublicKey.class, (o, out) -> out.writeBytes(((BlockchainPublicKey) o).asBytes()));
    map.put(
        BlsPublicKey.class,
        (o, out) -> out.writeBytes(((BlsPublicKey) o).getPublicKeyValue().serialize(true)));
    map.put(
        BlsSignature.class,
        (o, out) -> out.writeBytes(((BlsSignature) o).getSignatureValue().serialize(true)));
    map.put(
        Signature.class,
        (o, out) ->
            out.writeBytes(
                SafeDataOutputStream.serialize(stream -> ((Signature) o).write(stream))));
    map.put(Hash.class, (o, out) -> out.writeBytes(((Hash) o).getBytes()));

    SIMPLE_TYPES = Map.copyOf(map);
  }

  private final ByteOutput out;

  /**
   * Default constructor.
   *
   * @param out Byte stream to write to
   */
  public StateAbiSerializer(ByteOutput out) {
    this.out = out;
  }

  /**
   * Write state.
   *
   * @param state state to be written
   */
  public void write(StateSerializable state) {
    writeObject(state, state.getClass());
  }

  /**
   * Serialize state into base64.
   *
   * @param state state to be serialized
   * @return base64 encoded string of the serialized state
   */
  public static byte[] serialize(StateSerializable state) {
    return LittleEndianByteOutput.serialize(
        out -> {
          StateAbiSerializer serializer = new StateAbiSerializer(out);
          serializer.write(state);
        });
  }

  /**
   * Serialize the state.
   *
   * <p>If the address type is public the whole state is serialized as the wasm data
   *
   * <p>If the address type is zk, the state is serialized based on reflection except for the field
   * 'openState', which is serialized as its wasm data.
   *
   * <p>Otherwise the state is serialized based on reflection.
   *
   * @param state state to be serialized
   * @param addressType type of address
   * @return the serialized state
   */
  public static byte[] serializeV2(StateSerializable state, BlockchainAddress.Type addressType) {
    if (addressType == BlockchainAddress.Type.CONTRACT_PUBLIC) {
      StateAccessor accessor = StateAccessor.create(state);
      return accessor.get("state").cast(LargeByteArray.class).getData();
    } else if (addressType == BlockchainAddress.Type.CONTRACT_ZK) {
      return serializeZkBinderState(state);
    } else {
      return serialize(state);
    }
  }

  private static byte[] serializeZkBinderState(StateSerializable state) {
    return LittleEndianByteOutput.serialize(
        out -> {
          StateAbiSerializer serializer = new StateAbiSerializer(out);
          serializer.writeZkBinderState(state);
        });
  }

  private void writeZkBinderState(StateSerializable state) {
    ExceptionConverter.run(
        () -> {
          Class<?> clazz = state.getClass();
          Field[] fields = getFields(clazz);
          for (Field field : fields) {
            Object childValue = field.get(state);
            Class<?> type = field.getType();
            if (field.getName().equals("openState")) {
              byte[] contractState =
                  StateAccessor.create(state)
                      .get("openState")
                      .get("openState")
                      .cast(LargeByteArray.class)
                      .getData();
              out.writeBytes(contractState);
            } else {
              writeType(childValue, type, field.getGenericType());
            }
          }
        });
  }

  /**
   * Serialize avl tree into bytes.
   *
   * @param avlTree avl tree to be serialized
   * @param genericType generic type of the avl tree
   * @return serialized bytes of the avl tree
   */
  public static byte[] serializeAvlTree(AvlTree<?, ?> avlTree, Type genericType) {
    return LittleEndianByteOutput.serialize(
        out -> {
          StateAbiSerializer serializer = new StateAbiSerializer(out);
          serializer.writeAvlTree(avlTree, genericType);
        });
  }

  private void writeObject(Object value, Class<?> clazz) {
    ExceptionConverter.run(
        () -> {
          Field[] fields = getFields(clazz);
          for (Field field : fields) {
            Object childValue = field.get(value);
            Class<?> type = field.getType();
            writeType(childValue, type, field.getGenericType());
          }
        });
  }

  private void writeType(Object value, Class<?> type, Type genericType) {
    if (type.isPrimitive()) {
      writeValue(value, type);
    } else {
      writeNullableType(value, type, genericType);
    }
  }

  private void writeNullableType(Object value, Class<?> type, Type genericType) {
    if (value == null) {
      out.writeBoolean(false);
    } else {
      out.writeBoolean(true);

      if (type.equals(AvlTree.class)) {
        writeAvlTree((AvlTree<?, ?>) value, genericType);
      } else if (type.equals(FixedList.class)) {
        writeFixedList((FixedList<?>) value, genericType);
      } else if (isSimpleSubObject(type)) {
        writeObject(value, type);
      } else {
        writeValue(value, type);
      }
    }
  }

  private <K extends Comparable<K>> void writeAvlTree(AvlTree<K, ?> value, Type genericType) {
    Type[] typeArguments = ((ParameterizedType) genericType).getActualTypeArguments();
    Class<?> keyType = (Class<?>) typeArguments[0];
    Class<?> valueType = (Class<?>) typeArguments[1];
    out.writeI32(value.size());
    Set<K> keys = value.keySet();
    for (var key : keys) {
      writeType(key, keyType, keyType);
      writeType(value.getValue(key), valueType, valueType);
    }
  }

  private void writeFixedList(FixedList<?> value, Type genericType) {
    Type[] typeArguments = ((ParameterizedType) genericType).getActualTypeArguments();
    Class<?> typeArgument = (Class<?>) typeArguments[0];
    out.writeI32(value.size());
    for (var element : value) {
      writeType(element, typeArgument, typeArgument);
    }
  }

  private void writeValue(Object o, Class<?> type) {
    StateAbiWriter producer = SIMPLE_TYPES.get(type);
    if (producer != null) {
      producer.write(o, out);
    } else /*if (type.isEnum())*/ {
      out.writeU8(((Enum<?>) o).ordinal());
    }
  }

  private static Field[] getFields(Class<?> clazz) {
    Field[] fields = clazz.getDeclaredFields();
    for (Field field : fields) {
      field.setAccessible(true);
    }
    fields =
        Arrays.stream(fields)
            .filter(field -> !Modifier.isTransient(field.getModifiers()))
            .filter(field -> !Modifier.isStatic(field.getModifiers()))
            .sorted(Comparator.comparing(Field::getName))
            .toArray(Field[]::new);
    for (Field field : fields) {
      verifyFieldType(clazz, field);
    }

    return fields;
  }

  private static void verifyFieldType(Class<?> clazz, Field field) {
    Class<?> type = field.getType();
    if (type.equals(AvlTree.class)) {
      Type genericType = field.getGenericType();
      if (genericType instanceof ParameterizedType) {
        Type[] arguments = ((ParameterizedType) genericType).getActualTypeArguments();
        ensureClassType(field, arguments[0]);
        ensureClassType(field, arguments[1]);
      } else {
        throw new IllegalArgumentException("AvlTree must be parameterized");
      }
    } else if (type.equals(FixedList.class)) {
      Type genericType = field.getGenericType();
      if (genericType instanceof ParameterizedType) {
        Type[] arguments = ((ParameterizedType) genericType).getActualTypeArguments();
        ensureClassType(field, arguments[0]);
      } else {
        throw new IllegalArgumentException("FixedList must be parameterized");
      }
    } else {
      verifyClassType(clazz, field, type);
    }
  }

  private static void ensureClassType(Field field, Type argument) {
    if (!(argument instanceof Class)) {
      throw new IllegalArgumentException("Type arguments cannot be parameterized");
    }
    verifyClassType(field.getDeclaringClass(), field, (Class<?>) argument);
  }

  private static void verifyClassType(Class<?> clazz, Field field, Class<?> type) {
    if (isIllegalGenericType(type)) {
      throw new IllegalArgumentException(
          "Unsupported type ["
              + type
              + "] "
              + "in class ["
              + clazz
              + "] "
              + "for field "
              + field.getName());
    }
  }

  private static boolean isIllegalGenericType(Class<?> type) {
    return !(isSimpleSubObject(type) || SIMPLE_TYPES.containsKey(type) || type.isEnum());
  }

  static boolean isSimpleSubObject(Class<?> type) {
    return !SIMPLE_TYPES.containsKey(type) && StateSerializable.class.isAssignableFrom(type);
  }

  /** Interface for writing simple state types. */
  interface StateAbiWriter {
    /**
     * Write object to byte output.
     *
     * @param o the object to be written
     * @param out the byte output to be written to
     */
    void write(Object o, ByteOutput out);
  }
}
