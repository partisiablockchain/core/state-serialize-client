package com.partisiablockchain.serialization;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.coverage.WithResource;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Map;
import org.assertj.core.api.Assertions;
import org.assertj.core.api.recursive.comparison.RecursiveComparisonConfiguration;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

/** Test. */
public final class StateObjectMapperTest {

  private static final ObjectMapper mapper = StateObjectMapper.createObjectMapper();
  private static final Path REFERENCE_FILES_PATH =
      getResourceSourcePath(StateExamples.class, "ref");
  private static boolean REGENERATE_REF_FILES = false;

  /** Initialize constant determining if reference files should be regenerated. */
  @BeforeAll
  public static void checkShouldRegenerateFiles() {
    REGENERATE_REF_FILES = !REFERENCE_FILES_PATH.toFile().exists();
  }

  final RecursiveComparisonConfiguration cryptoAwareConfig =
      RecursiveComparisonConfiguration.builder()
          .withEqualsForType(BlsPublicKey::equals, BlsPublicKey.class)
          .withEqualsForType(BlsSignature::equals, BlsSignature.class)
          .withStrictTypeChecking(true)
          .build();

  @Test
  public void regenerateRefFilesIsFalse() {
    Assertions.assertThat(REGENERATE_REF_FILES).isFalse();
  }

  @Test
  public void serializeStateLong() throws JsonProcessingException {
    WithLongs value = new WithLongs(13L, 14);
    JsonNode s = mapper.valueToTree(value);
    Assertions.assertThat(s.has("nullable")).isTrue();
    Assertions.assertThat(s.get("nullable").isTextual()).isTrue();
    Assertions.assertThat(s.has("nonNull")).isTrue();
    Assertions.assertThat(s.get("nonNull").isTextual()).isTrue();

    WithLongs withLongs = mapper.treeToValue(s, WithLongs.class);
    Assertions.assertThat(withLongs.nullable).isEqualTo(value.nullable);
    Assertions.assertThat(withLongs.nonNull).isEqualTo(value.nonNull);
  }

  @Test
  void serializeAvlTree() throws JsonProcessingException {
    BlockchainAddress address1 =
        BlockchainAddress.fromString("000000000000000000000000000000000000000001");
    BlockchainAddress address2 =
        BlockchainAddress.fromString("000000000000000000000000000000000000000002");

    AvlTree<BlockchainAddress, Long> reference =
        AvlTree.create(
            Map.of(
                address1, 1L,
                address2, 2L));

    JsonNode asJson = mapper.valueToTree(reference);
    AvlTree<BlockchainAddress, Long> result = mapper.treeToValue(asJson, new TypeReference<>() {});

    Assertions.assertThat(result).usingRecursiveComparison().isEqualTo(reference);
  }

  /**
   * Reference test that the given test case can be serialized to a well-defined JSON form.
   *
   * @param testCase Test case.
   */
  @ParameterizedTest
  @MethodSource("provideAllTestCases")
  public void writeReferenceJson(final StateExamples.TestCase testCase) {
    // Setup
    final Path referenceFilepath = REFERENCE_FILES_PATH.resolve(testCase.name() + ".json");
    final StateSerializable stateObject = testCase.state();
    final ObjectMapper jsonMapper = StateObjectMapper.createObjectMapper();

    // Determine produced output
    String gottenOutput =
        ExceptionConverter.call(
            () -> jsonMapper.writeValueAsString(stateObject), "Unable to convert to json");

    // Write reference if regenerating
    if (REGENERATE_REF_FILES) {
      writeText(referenceFilepath, gottenOutput);
      System.err.println("Regenerated " + referenceFilepath);
    }

    // Load reference text
    final String expectedOutput = loadText(referenceFilepath);

    // Check output is as expected
    Assertions.assertThat(gottenOutput)
        .as(testCase.name())
        .isEqualToIgnoringNewLines(expectedOutput);
  }

  @ParameterizedTest
  @MethodSource("provideAllTestCases")
  void writeReadJson(final StateExamples.TestCase testCase) {
    // Setup
    System.out.println(testCase.name());
    final Class<? extends StateSerializable> clazz = testCase.state().getClass();
    final StateSerializable stateObject = testCase.state();
    final Class<?>[] parameters = testCase.typeArguments().toArray(new Class<?>[0]);
    final ObjectMapper jsonMapper = StateObjectMapper.createObjectMapper();

    // Write to json
    final String stateAsJson =
        ExceptionConverter.call(
            () -> jsonMapper.writeValueAsString(stateObject),
            testCase.name() + ": Unable to convert to json");

    // Read from json
    final Object stateReadFromJson =
        ExceptionConverter.call(
            () ->
                jsonMapper.readValue(
                    stateAsJson,
                    jsonMapper.getTypeFactory().constructParametricType(clazz, parameters)),
            testCase.name() + ": Unable to read from json");

    // Verify that state is stable across serialization
    Assertions.assertThat(stateObject)
        .as(testCase.name())
        .usingRecursiveComparison(cryptoAwareConfig)
        .isEqualTo(stateReadFromJson);
  }

  /**
   * Writes text to the given file.
   *
   * @param filePath Path of file.
   * @param textWithUnixNewline Text contents of file, with unix newlines.
   */
  static void writeText(final Path filePath, final String textWithUnixNewline) {
    final String text = textWithUnixNewline.replace("\n", System.lineSeparator());

    WithResource.accept(
        () -> {
          // Create any directories required
          Files.createDirectories(filePath.getParent());

          // Write file itself
          return new java.io.PrintWriter(filePath.toFile(), StandardCharsets.UTF_8);
        },
        outputStream -> outputStream.write(text),
        "Could not write to file " + filePath);
  }

  /**
   * Loads text from the given file.
   *
   * @param filePath Path of file.
   * @return Text contents of file.
   */
  static String loadText(final Path filePath) {
    final byte[] fileBytes =
        WithResource.apply(
            () -> new FileInputStream(filePath.toFile()),
            InputStream::readAllBytes,
            "Could not load file: " + filePath);

    return new String(fileBytes, StandardCharsets.UTF_8);
  }

  /**
   * Produces path for the resource with the given filename relative to the given class.
   *
   * @param clazz Class to determine resource relative to.
   * @param filename Filename of resource.
   * @return Path of file.
   */
  static Path getResourceSourcePath(final Class<?> clazz, final String filename) {
    final String[] split = clazz.getPackageName().split("\\.");
    final Path referencePathDir = Paths.get("./src/test/resources/", split);
    return referencePathDir.resolve(filename);
  }

  private static final class WithLongs {

    final Long nullable;
    final long nonNull;

    @SuppressWarnings("unused")
    private WithLongs() {
      nullable = null;
      nonNull = 0;
    }

    private WithLongs(Long nullable, long nonNull) {
      this.nullable = nullable;
      this.nonNull = nonNull;
    }
  }

  static Collection<StateExamples.TestCase> provideAllTestCases() {
    return StateExamples.provideAllTestCases();
  }
}
