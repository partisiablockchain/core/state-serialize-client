package com.partisiablockchain.server.rest;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsKeyPair;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Curve;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateBoolean;
import com.partisiablockchain.serialization.StateLong;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.serialization.StateString;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.LittleEndianByteOutput;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.immutable.FixedList;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.HexFormat;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@SuppressWarnings({"rawtypes", "unused"})
final class StateAbiSerializerTest {

  private static final byte[] ENCODED_EC_POINT =
      Curve.CURVE.getG().multiply(BigInteger.valueOf(777)).getEncoded(false);
  private static final BlsKeyPair BLS_KEY_PAIR = new BlsKeyPair(BigInteger.TEN);

  @SuppressWarnings("FieldCanBeLocal")
  @Immutable
  static final class AllTypesState implements StateSerializable {
    // Fields are sorted alphabetically
    private final byte aaByte;
    private final Byte bbByte;
    private final short ccShort;
    private final Short ddShort;
    private final int eeInt;
    private final Integer ffInt;
    private final long ggLong;
    private final Long hhLong;
    private final StateLong iiStateLong;
    private final String jjString;
    private final StateString kkStateString;
    private final boolean llBool;
    private final Boolean mmBool;
    private final StateBoolean nnStateBool;
    private final BlockchainAddress ooAddress;
    private final LargeByteArray ppLargeArray;
    private final Unsigned256 qqUnsigned256;
    private final BlockchainPublicKey rrBlockchainPublicKey;
    private final BlsPublicKey ssBlsPublicKey;
    private final BlsSignature ttBlsSignature;
    private final Signature uuSignature;
    private final Hash vvHash;
    private final String wwNullString;

    AllTypesState(
        byte aaByte,
        Byte bbByte,
        short ccShort,
        Short ddShort,
        int eeInt,
        Integer ffInt,
        long ggLong,
        Long hhLong,
        StateLong iiStateLong,
        String jjString,
        StateString kkStateString,
        boolean llBool,
        Boolean mmBool,
        StateBoolean nnStateBool,
        BlockchainAddress ooAddress,
        LargeByteArray ppLargeArray,
        Unsigned256 qqUnsigned256,
        BlockchainPublicKey rrBlockchainPublicKey,
        BlsPublicKey ssBlsPublicKey,
        BlsSignature ttBlsSignature,
        Signature uuSignature,
        Hash vvHash,
        String wwNullString) {
      this.aaByte = aaByte;
      this.bbByte = bbByte;
      this.ccShort = ccShort;
      this.ddShort = ddShort;
      this.eeInt = eeInt;
      this.ffInt = ffInt;
      this.ggLong = ggLong;
      this.hhLong = hhLong;
      this.iiStateLong = iiStateLong;
      this.jjString = jjString;
      this.kkStateString = kkStateString;
      this.llBool = llBool;
      this.mmBool = mmBool;
      this.nnStateBool = nnStateBool;
      this.ooAddress = ooAddress;
      this.ppLargeArray = ppLargeArray;
      this.qqUnsigned256 = qqUnsigned256;
      this.rrBlockchainPublicKey = rrBlockchainPublicKey;
      this.ssBlsPublicKey = ssBlsPublicKey;
      this.ttBlsSignature = ttBlsSignature;
      this.uuSignature = uuSignature;
      this.vvHash = vvHash;
      this.wwNullString = wwNullString;
    }
  }

  @Test
  void contractAllTypes() {
    Signature signature =
        new Signature(2, BigInteger.valueOf(115599L), BigInteger.valueOf(779995566L));
    AllTypesState exampleContractState =
        new AllTypesState(
            (byte) 1,
            (byte) 2,
            (short) 3,
            (short) 4,
            5,
            6,
            7L,
            8L,
            new StateLong(9L),
            "Hello",
            new StateString("World"),
            true,
            false,
            new StateBoolean(true),
            BlockchainAddress.fromString("000000000000000000000000000000000000000042"),
            new LargeByteArray(new byte[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}),
            Unsigned256.create(16L),
            BlockchainPublicKey.fromEncodedEcPoint(ENCODED_EC_POINT),
            BLS_KEY_PAIR.getPublicKey(),
            BLS_KEY_PAIR.sign(Hash.create(s -> s.writeString("message"))),
            signature,
            Hash.fromString("0400000000000000000000000000000000000000000000000000000000000004"),
            null);

    byte[] bytes =
        LittleEndianByteOutput.serialize(
            out -> {
              StateAbiSerializer serializer = new StateAbiSerializer(out);
              serializer.write(exampleContractState);
            });

    String publicKeyHex =
        HexFormat.of()
            .formatHex(BlockchainPublicKey.fromEncodedEcPoint(ENCODED_EC_POINT).asBytes());
    String blsPublicKeyHex =
        HexFormat.of()
            .formatHex(
                SafeDataOutputStream.serialize(
                    (stream) -> BLS_KEY_PAIR.getPublicKey().write(stream)));
    String blsSignatureHex =
        HexFormat.of()
            .formatHex(
                SafeDataOutputStream.serialize(
                    (stream) ->
                        BLS_KEY_PAIR
                            .sign(Hash.create(s -> s.writeString("message")))
                            .write(stream)));

    assertThat(bytes)
        .isEqualTo(
            HexFormat.of()
                .parseHex(
                    "01" // aaByte 1
                        + "0102" // bbByte 2
                        + "0300" // ccShort 3
                        + "010400" // ddShort 4
                        + "05000000" // eeInt 5
                        + "0106000000" // ffInt 6
                        + "0700000000000000" // ggLong 7
                        + "010800000000000000" // hhLong 8
                        + "010900000000000000" // iiStateLong 9
                        + "010500000048656C6C6F" // jjString: Hello
                        + "0105000000576F726C64" // kkStateString: World
                        + "01" // llBool: primitive true
                        + "0100" // mmBool: false
                        + "0101" // nnStateBool: true
                        + "01000000000000000000000000000000000000000042" // ooAddress
                        + "0110000000000102030405060708090a0b0c0d0e0f" // pLargeByteArray
                        + "011000000000000000000000000000000000000000000000000000000000000000"
                        // qqUnsigned256: 16
                        + "01"
                        + publicKeyHex // rrBlockchainPublicKey
                        + "01"
                        + blsPublicKeyHex // ssBlsPublicKey
                        + "01"
                        + blsSignatureHex // ttBlsSignature
                        + "01"
                        + signature.writeAsString() // uuSignature
                        + "01"
                        + Hash.fromString(
                            "0400000000000000000000000000000000000000000000000000000000000004")
                        // vvHash
                        + "00" // wNull
                    ));
  }

  @SuppressWarnings("FieldCanBeLocal")
  @Immutable
  static final class ExampleClass implements StateSerializable {
    private final boolean myBool;
    private final int state;
    private static final int ignoreStatic = 3;
    private final transient int ignoreTransient = 4;

    ExampleClass(boolean myBool, int state) {
      this.myBool = myBool;
      this.state = state;
    }
  }

  @SuppressWarnings("FieldCanBeLocal")
  @Immutable
  static final class ExampleClassField implements StateSerializable {
    private final ExampleClass myFieldClass;
    private final int myInt;

    ExampleClassField(ExampleClass myFieldClass, int myInt) {
      this.myFieldClass = myFieldClass;
      this.myInt = myInt;
    }
  }

  @SuppressWarnings("FieldCanBeLocal")
  @Immutable
  static final class ExampleListClass implements StateSerializable {
    private final boolean myBool;
    private final FixedList<Integer> myList;

    ExampleListClass(boolean myBool, FixedList<Integer> myList) {
      this.myBool = myBool;
      this.myList = myList;
    }
  }

  @SuppressWarnings("FieldCanBeLocal")
  @Immutable
  static final class ExampleTreeClass implements StateSerializable {
    private final boolean myBool;
    private final AvlTree<Integer, Boolean> myTree;

    ExampleTreeClass(boolean myBool, AvlTree<Integer, Boolean> myTree) {
      this.myBool = myBool;
      this.myTree = myTree;
    }
  }

  @Test
  void fieldClass() {
    ExampleClass exampleClass = new ExampleClass(true, 0x42);
    ExampleClassField exampleClassField = new ExampleClassField(exampleClass, 1);
    byte[] bytes =
        LittleEndianByteOutput.serialize(
            out -> {
              StateAbiSerializer serializer = new StateAbiSerializer(out);
              serializer.write(exampleClassField);
            });

    assertThat(bytes).isEqualTo(HexFormat.of().parseHex("01014200000001000000"));
  }

  @Test
  void listClass() {
    ExampleListClass exampleClass = new ExampleListClass(true, FixedList.create(List.of(1, 2, 3)));
    byte[] bytes =
        LittleEndianByteOutput.serialize(
            out -> {
              StateAbiSerializer serializer = new StateAbiSerializer(out);
              serializer.write(exampleClass);
            });
    assertThat(bytes)
        .isEqualTo(HexFormat.of().parseHex("010103000000010100000001020000000103000000"));
  }

  @Test
  void treeClass() {
    AvlTree<Integer, Boolean> tree = AvlTree.create();
    tree = tree.set(1, false);
    tree = tree.set(2, false);
    tree = tree.set(3, true);
    ExampleTreeClass exampleClass = new ExampleTreeClass(true, tree);
    byte[] bytes =
        LittleEndianByteOutput.serialize(
            out -> {
              StateAbiSerializer serializer = new StateAbiSerializer(out);
              serializer.write(exampleClass);
            });
    assertThat(bytes)
        .isEqualTo(
            HexFormat.of().parseHex("010103000000010100000001000102000000010001030000000101"));
  }

  @SuppressWarnings("FieldCanBeLocal")
  @Immutable
  static final class ExampleEnumClass implements StateSerializable {
    private final MyEnum myEnum;

    ExampleEnumClass(MyEnum myEnum) {
      this.myEnum = myEnum;
    }

    enum MyEnum {
      SATURDAY,
      SUNDAY
    }
  }

  @Test
  void enumClass() {
    ExampleEnumClass exampleEnumClass = new ExampleEnumClass(ExampleEnumClass.MyEnum.SUNDAY);
    byte[] bytes =
        LittleEndianByteOutput.serialize(
            out -> {
              StateAbiSerializer serializer = new StateAbiSerializer(out);
              serializer.write(exampleEnumClass);
            });
    assertThat(bytes).isEqualTo(HexFormat.of().parseHex("0101"));
  }

  @SuppressWarnings("FieldCanBeLocal")
  @Immutable
  static final class ListMustBeParameterized implements StateSerializable {
    @SuppressWarnings({"Immutable"})
    private final FixedList myList;

    ListMustBeParameterized(FixedList myList) {
      this.myList = myList;
    }
  }

  @SuppressWarnings("FieldCanBeLocal")
  @Immutable
  static final class TreeMustBeParameterized implements StateSerializable {
    @SuppressWarnings({"Immutable"})
    private final AvlTree myTree;

    TreeMustBeParameterized(AvlTree myTree) {
      this.myTree = myTree;
    }
  }

  @SuppressWarnings("FieldCanBeLocal")
  @Immutable
  static final class TypeArgumentsCannotBeParameterized implements StateSerializable {
    private final FixedList<FixedList<Integer>> myList;

    TypeArgumentsCannotBeParameterized(FixedList<FixedList<Integer>> myList) {
      this.myList = myList;
    }
  }

  @SuppressWarnings("FieldCanBeLocal")
  @Immutable
  static final class IllegalField implements StateSerializable {

    @SuppressWarnings("Immutable")
    private final ByteBuffer buffer;

    IllegalField(ByteBuffer buffer) {
      this.buffer = buffer;
    }
  }

  @SuppressWarnings("FieldCanBeLocal")
  @Immutable
  static final class IllegalKey implements StateSerializable {

    @SuppressWarnings("Immutable")
    private final AvlTree<ByteBuffer, Integer> tree;

    IllegalKey(AvlTree<ByteBuffer, Integer> tree) {
      this.tree = tree;
    }
  }

  @SuppressWarnings("FieldCanBeLocal")
  @Immutable
  static final class IllegalValue implements StateSerializable {

    @SuppressWarnings("Immutable")
    private final AvlTree<Integer, ByteBuffer> tree;

    IllegalValue(AvlTree<Integer, ByteBuffer> tree) {
      this.tree = tree;
    }
  }

  @Test
  void illegalTypeParameters() {
    StateAbiSerializer serializer =
        new StateAbiSerializer(new LittleEndianByteOutput(new ByteArrayOutputStream()));
    assertThatThrownBy(() -> serializer.write(new ListMustBeParameterized(null)))
        .isInstanceOf(RuntimeException.class)
        .hasStackTraceContaining("FixedList must be parameterized");
    assertThatThrownBy(() -> serializer.write(new TreeMustBeParameterized(null)))
        .isInstanceOf(RuntimeException.class)
        .hasStackTraceContaining("AvlTree must be parameterized");
    assertThatThrownBy(() -> serializer.write(new TypeArgumentsCannotBeParameterized(null)))
        .isInstanceOf(RuntimeException.class)
        .hasStackTraceContaining("Type arguments cannot be parameterized");
    assertThatThrownBy(() -> serializer.write(new IllegalField(null)))
        .isInstanceOf(RuntimeException.class)
        .hasStackTraceContaining(
            "Unsupported type [class java.nio.ByteBuffer] in class "
                + "[class "
                + IllegalField.class.getName()
                + "] "
                + "for field buffer");
    assertThatThrownBy(() -> serializer.write(new IllegalKey(null)))
        .isInstanceOf(RuntimeException.class)
        .hasStackTraceContaining(
            "Unsupported type [class java.nio.ByteBuffer] in class "
                + "[class "
                + IllegalKey.class.getName()
                + "] "
                + "for field tree");
    assertThatThrownBy(() -> serializer.write(new IllegalValue(null)))
        .isInstanceOf(RuntimeException.class)
        .hasStackTraceContaining(
            "Unsupported type [class java.nio.ByteBuffer] in class "
                + "[class "
                + IllegalValue.class.getName()
                + "] "
                + "for field tree");
  }

  @SuppressWarnings("FieldCanBeLocal")
  @Immutable
  static final class WasmContract implements StateSerializable {

    private final LargeByteArray state;

    WasmContract(LargeByteArray state) {
      this.state = state;
    }
  }

  @Test
  void createContract() throws IOException {
    JsonNode serializedContract =
        JsonNodeFactory.instance.binaryNode(
            StateAbiSerializer.serialize(new ExampleClass(true, 0x42)));
    assertThat(serializedContract.binaryValue()).isEqualTo(HexFormat.of().parseHex("0142000000"));
  }

  record AvlWrap(AvlTree<Integer, Boolean> avlTree) {}

  @Test
  void serializeAvlTree() throws NoSuchFieldException {
    AvlTree<Integer, Boolean> avl = AvlTree.create();
    avl = avl.set(1, true);
    avl = avl.set(2, false);

    byte[] avlBytes =
        StateAbiSerializer.serializeAvlTree(
            avl, AvlWrap.class.getDeclaredField("avlTree").getGenericType());
    assertThat(avlBytes).isEqualTo(HexFormat.of().parseHex("020000000101000000010101020000000100"));
  }

  @SuppressWarnings("FieldCanBeLocal")
  @Immutable
  static final class WasmContractAvl implements StateSerializable {

    private final LargeByteArray state;
    private final AvlTree<Integer, Integer> avlTrees;

    WasmContractAvl(LargeByteArray state, AvlTree<Integer, Integer> avlTrees) {
      this.state = state;
      this.avlTrees = avlTrees;
    }
  }

  @Test
  void createContractAvl() {
    byte[] serializedContract = StateAbiSerializer.serialize(new ExampleClass(true, 0x42));
    assertThat(serializedContract).isEqualTo(HexFormat.of().parseHex("0142000000"));

    byte[] encodedState = HexFormat.of().parseHex("0142000000");

    WasmContractAvl state = new WasmContractAvl(new LargeByteArray(encodedState), AvlTree.create());
    byte[] stateBytes =
        StateAccessor.create(state).get("state").cast(LargeByteArray.class).getData();

    AvlTree<?, ?> avlTree = StateAccessor.create(state).get("avlTrees").cast(AvlTree.class);
    Type genericType =
        ExceptionConverter.call(
            () -> state.getClass().getDeclaredField("avlTrees").getGenericType());
    byte[] avlBytes = StateAbiSerializer.serializeAvlTree(avlTree, genericType);

    assertThat(stateBytes).containsExactly(encodedState);
    assertThat(avlBytes).containsExactly(HexFormat.of().parseHex("00000000"));

    StateAccessor stateAccessor =
        StateAccessor.create(new WasmContract(new LargeByteArray(encodedState)));
    stateBytes = stateAccessor.get("state").cast(LargeByteArray.class).getData();
    assertThat(stateBytes).containsExactly(encodedState);
  }

  @SuppressWarnings("FieldCanBeLocal")
  @Immutable
  static final class OpenWasmContractAvl implements StateSerializable {

    private final LargeByteArray openState;
    private final AvlTree<Integer, Integer> avlTrees;

    OpenWasmContractAvl(LargeByteArray openState, AvlTree<Integer, Integer> avlTrees) {
      this.openState = openState;
      this.avlTrees = avlTrees;
    }
  }

  @SuppressWarnings("FieldCanBeLocal")
  @Immutable
  static final class ZkWasmContractAvl implements StateSerializable {

    private final OpenWasmContractAvl openState;

    ZkWasmContractAvl(OpenWasmContractAvl openState) {
      this.openState = openState;
    }
  }

  @SuppressWarnings("FieldCanBeLocal")
  @Immutable
  static final class OpenWasmContract implements StateSerializable {

    private final LargeByteArray openState;

    OpenWasmContract(LargeByteArray openState) {
      this.openState = openState;
    }
  }

  @SuppressWarnings("FieldCanBeLocal")
  @Immutable
  static final class ZkWasmContract implements StateSerializable {

    private final OpenWasmContract openState;

    ZkWasmContract(OpenWasmContract openState) {
      this.openState = openState;
    }
  }

  @Test
  void createContractAvlZk() {
    byte[] serializedContract = StateAbiSerializer.serialize(new ExampleClass(true, 0x42));

    byte[] encodedState = HexFormat.of().parseHex("0142000000");

    ZkWasmContractAvl zkWasmContractAvl =
        new ZkWasmContractAvl(
            new OpenWasmContractAvl(new LargeByteArray(encodedState), AvlTree.create()));
    StateAccessor stateAccessor = StateAccessor.create(zkWasmContractAvl).get("openState");
    byte[] stateBytes = stateAccessor.get("openState").cast(LargeByteArray.class).getData();

    StateSerializable state = stateAccessor.cast(StateSerializable.class);
    AvlTree<?, ?> avlTree = StateAccessor.create(state).get("avlTrees").cast(AvlTree.class);
    Type genericType =
        ExceptionConverter.call(
            () -> state.getClass().getDeclaredField("avlTrees").getGenericType());
    byte[] avlBytes = StateAbiSerializer.serializeAvlTree(avlTree, genericType);

    assertThat(stateBytes).containsExactly(encodedState);
    assertThat(avlBytes).containsExactly(HexFormat.of().parseHex("00000000"));

    ZkWasmContract zkWasmContract =
        new ZkWasmContract(new OpenWasmContract(new LargeByteArray(encodedState)));
    stateAccessor = StateAccessor.create(zkWasmContract).get("openState");
    stateBytes = stateAccessor.get("openState").cast(LargeByteArray.class).getData();
    assertThat(stateBytes).containsExactly(encodedState);
  }

  @DisplayName("A public wasm contract is serialized as just the wasm state.")
  @Test
  void serializeV2PubContract() {
    byte[] stateBytes = {1, 2, 3};
    var state = new WasmContractAvl(new LargeByteArray(stateBytes), AvlTree.create());
    byte[] serializedBytes =
        StateAbiSerializer.serializeV2(state, BlockchainAddress.Type.CONTRACT_PUBLIC);
    assertThat(serializedBytes).isEqualTo(stateBytes);
  }

  record ZkBinderState(OpenWasmContractAvl openState, int otherBinderState)
      implements StateSerializable {}

  @DisplayName(
      "A zk wasm contract is serialized reflection based except for its openState field which is"
          + " serialized as its wasm state data.")
  @Test
  void serializeV2ZkContract() {
    byte[] stateBytes = {1, 2, 3};
    var state =
        new ZkBinderState(
            new OpenWasmContractAvl(new LargeByteArray(stateBytes), AvlTree.create()), 9);
    byte[] serializedBytes =
        StateAbiSerializer.serializeV2(state, BlockchainAddress.Type.CONTRACT_ZK);
    assertThat(serializedBytes).isEqualTo(HexFormat.of().parseHex("01020309000000"));
  }

  @DisplayName("A governance contract is serialized reflection based.")
  @Test
  void serializeGovContract() {
    ExampleClass exampleClass = new ExampleClass(true, 4);
    byte[] bytes =
        StateAbiSerializer.serializeV2(exampleClass, BlockchainAddress.Type.CONTRACT_GOV);
    assertThat(bytes).isEqualTo(HexFormat.of().parseHex("0104000000"));
  }
}
